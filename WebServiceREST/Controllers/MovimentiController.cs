﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using WebServiceREST.Context;

namespace WebServiceREST.Controllers
{

    [EnableCors(origins: "http://localhost:8567", headers: "*", methods: "*")]
    [RoutePrefix("movimenti")]
    public class MovimentiController : ApiController
    {
        DataContext _data;

        public MovimentiController()
        {
            _data = new DataContext();
        }

        // GET: Movimenti
        [Route("List")]
        public HttpResponseMessage GetAll()
        {
            var result = _data.prodotto.ToArray();
            var formatter = new JsonMediaTypeFormatter();
            var json = formatter.SerializerSettings;

            var jsonData = new
            {
                total = "1",
                page = "1",
                records = "1",
                rows = _data.movimenti.Select(p => new {
                    p.Id,
                    p.Data,
                    p.Segno,
                    p.Quantita,
                    p.prodotto.Nome,
                    p.magazzino.Descrizione
                }).ToArray().Select( x => new {
                    Id = x.Id,
                    Data = string.Format("{0:dd/MM/yyyy}", x.Data),
                    Segno = x.Segno,
                    Quantita = x.Quantita,
                    Prodotto = x.Nome,
                    Magazzino = x.Descrizione
                })
            };

            json.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            return Request.CreateResponse(HttpStatusCode.OK, jsonData, formatter);
        }

    }
}