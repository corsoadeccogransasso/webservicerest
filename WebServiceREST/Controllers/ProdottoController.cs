﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Cors;
using WebServiceREST.Context;
using WebServiceREST.Domain;

namespace WebServiceREST.Controllers
{
    [EnableCors(origins: "http://localhost:8567", headers: "*", methods: "*")]
    [RoutePrefix("Prodotto")]
    public class ProdottoController : ApiController
    {

        DataContext _data;

        public ProdottoController()
        {
            _data = new DataContext();
        }

        [Route("List")]
        public HttpResponseMessage GetAll()
        {
            var result = _data.prodotto.ToArray();
            var formatter = new JsonMediaTypeFormatter();
            var json = formatter.SerializerSettings;

            var jsonData = new
            {
                total = "1",
                page = "1",
                records = "1",
                rows = _data.prodotto.ToArray()
            };

            return Request.CreateResponse(HttpStatusCode.OK, jsonData, formatter);
        }

        // GET api/<controller>/5
        [Route("List")]
        public Prodotto GetById(int id)
        {
            return _data.prodotto.Where(p=>p.Id==id).FirstOrDefault();
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}