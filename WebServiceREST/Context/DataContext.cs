﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using WebServiceREST.Domain;

namespace WebServiceREST.Context
{
        public class DataContext : DbContext
        {

            public DbSet<Prodotto> prodotto { get; set; }
            public DbSet<Magazzino> magazzino { get; set; }
            public DbSet<Movimenti> movimenti { get; set; }

            public DataContext() : base("GranSassoDB")
            {
            }

            protected override void OnModelCreating(DbModelBuilder modelBuilder)
            {
                modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
                base.OnModelCreating(modelBuilder);
            }
        }
}