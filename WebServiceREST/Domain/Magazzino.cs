﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServiceREST.Domain
{
    public class Magazzino: IIdentity
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descrizione { get; set; }
        public bool Cancellato { get; set; }

        public ICollection<Movimenti> ListaMovimenti { get; set; }

    }
}