﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServiceREST.Domain
{
    public class Movimenti:IIdentity
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public string Note { get; set; }
        public string Segno { get; set; }
        public int Quantita { get; set; }

        public Prodotto prodotto  { get; set; }
        public Magazzino magazzino { get; set; }
    }
}