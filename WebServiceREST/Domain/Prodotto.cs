﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebServiceREST.Domain
{
    public class Prodotto:IIdentity
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Display(Name ="Nome del Prodotto:")]
        public string Nome { get; set; }

        public string Codice { get; set; }

        public string MagazzinoDefault { get; set; }
        public decimal Prezzo { get; set; }
        public bool cancellato { get; set; }

        public ICollection<Movimenti> ListaMovimenti { get; set; }

    }
}